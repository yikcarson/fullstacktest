import React from "react";
import { IRootState, ThunkDispatch } from "../../store";
import { connect } from "react-redux";
import moment from 'moment'

interface ISuccessProps {
    success: boolean,
    deliveryTime: number,
}

class Success extends React.Component<ISuccessProps> {
    get = () => {
        if (this.props.success) {
            return 'Order Success'
        } else {
            return "Order Faile"
        }
    }

    render() {
        return (
            <div id='Success'>
                <div> OrderStatus:{`${this.get()}`}</div>
                <div>DeliveryTime:{this.props.success===true?moment().endOf('hours').fromNow():null}</div>
            </div>)


    }
}
const mapStateToProps = (state: IRootState) => ({
    success: state.pizzas.success,
    deliveryTime: state.pizzas.deliveryTime
})
const mapDispatchToProps = (dispatch: ThunkDispatch) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(Success)