import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusSquare, faMinus } from '@fortawesome/free-solid-svg-icons'

interface IPizzaPros {
    name: string,
    price: number,
    quantity: number,
    ingredients: string[]
    add(): void
    reduce(): void
}

export class Pizza extends React.Component<IPizzaPros> {

    render() {
        return (
            <div className='Pizza'>
                <div id='PizzaName'>{this.props.name}</div>
                <div id='PizzaHeader'>
                    <span>Ingredient</span>
                    <span>Price</span>
                    <span>Amount</span>
                    <span>Add/Minus</span>
                </div>
                <div id="PizzaContent">
                    <div id='PizzaContent_ingredients'>{this.props.ingredients.map((e,index)=>{return <div>{e}</div> })}</div>
                    <div><span>${this.props.price}</span></div>
                    <div><span>{this.props.quantity}</span></div>
                    <div id='PizzaButton'><span onClick={this.props.add}><FontAwesomeIcon icon={faPlusSquare} size='lg'/></span>
                        <span onClick={this.props.reduce}><FontAwesomeIcon icon={faMinus} size='lg'/></span>
                    </div>

                </div>
            </div>
        )
    }
}