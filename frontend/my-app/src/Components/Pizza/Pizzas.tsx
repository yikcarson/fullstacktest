import * as React from 'react'
import { Pizza } from './Pizza';
import { pizza, ClientOrder } from '../../model/pizzaModel';
import { Redirect } from 'react-router-dom';
import { IRootState, ThunkDispatch } from '../../store';
import { submitOrder } from '../../redux/Pizzas/actions';
import { connect } from 'react-redux';

interface IPizzasState {
    pizzasDatas: pizza[],
    totalPrice: number,
    redirect:boolean
}

interface IPizzasProps {
    redirect: boolean
    submit: (postDatas: ClientOrder[]) => void
}

class Pizzas extends React.Component<IPizzasProps, IPizzasState>{
    state = {
        pizzasDatas: [] as pizza[],
        totalPrice: 0,
        redirect: false
    }
    componentDidMount = async () => {
        const res = await fetch('http://localhost:8080/pizzas', {
            method: 'GET',
        })
        const result: pizza[] = await res.json()
        result.map(pizzaData => {
            pizzaData.quantity = 0
            this.setState({
                pizzasDatas: this.state.pizzasDatas.concat(pizzaData)
            })

        })
    }

    add = (name: string) => {
        const hasData = this.state.pizzasDatas.find(key => key.name == name)
        if (hasData) {
            hasData.quantity++
            this.setState({
                pizzasDatas: [...this.state.pizzasDatas],
                totalPrice: this.state.totalPrice + hasData.price
            })
        }
    }

    reduce = (name: string) => {
        const hasData = this.state.pizzasDatas.find(key => key.name == name)
        if (hasData && hasData.quantity > 0) {
            hasData.quantity--
            this.setState({
                pizzasDatas: [...this.state.pizzasDatas],
                totalPrice: this.state.totalPrice - hasData.price
            })
        } else {
            window.alert('quantity is less than 0')
        }
    }

    submit = async () => {
        const postDatas = this.state.pizzasDatas.reduce((postDatas: ClientOrder[], currentdata) => {
            if (currentdata.quantity > 0) {
                postDatas.push({ name: currentdata.name, quantity: currentdata.quantity })
            }
            return postDatas
        }, [])
        if (postDatas.length == 0) {
            return window.alert('pleass make a choose')
        }
        this.props.submit(postDatas)
        this.setState({
            redirect:true
        })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to="/order/success" />;
        }
        return (

            <div id="pizzas">
                {this.state.pizzasDatas.map(pizzaComponent => {
                    return <Pizza
                        key={pizzaComponent.name}
                        name={pizzaComponent.name}
                        price={pizzaComponent.price}
                        quantity={pizzaComponent.quantity}
                        ingredients={pizzaComponent.ingredients}
                        add={() => this.add(pizzaComponent.name)}
                        reduce={() => this.reduce(pizzaComponent.name)}
                    />
                })}
                <div id="ButtonGroup">
                    <div id="TotalPrice"><div>Total Price:${this.state.totalPrice}</div></div>
                    <button id='SubmitButton' onClick={this.submit}> <div>submit</div></button>
                </div>
            </div >)

    }
}
const mapStateToProps = (state: IRootState) => ({
    redirect: state.pizzas.redirect
})
const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    submit: (postDatas: ClientOrder[]) => { dispatch(submitOrder(postDatas)) }
})

export default connect(mapStateToProps, mapDispatchToProps)(Pizzas)