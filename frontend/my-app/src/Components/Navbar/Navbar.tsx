import React from 'react';
import { Button } from 'reactstrap'
import { NavLink } from 'react-router-dom';


export class Navbar extends React.Component {
    render() {
        return (
            <div>
                <NavLink to={'/'}><Button className='NavbarButton' color="secondary" > Client</Button></NavLink>
                <NavLink to={'/admin'}><Button className='NavbarButton' color="secondary" > Admin</Button></NavLink>
            </div>
        )
    }
}