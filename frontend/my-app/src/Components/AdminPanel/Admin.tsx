import * as React from 'react';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from '../../store';
import { AllOrders } from '../../model/AdminOrder';
import { GetAllOrders } from '../../redux/AdminPanel/action';
import OrderBox from './OrderBox';

interface IAdminProps {
    AllOrders: AllOrders,
    upDateData: () => void
}

class Admin extends React.Component<IAdminProps> {

    componentDidMount = () => {
        this.props.upDateData()
    }
    render() {
        return (
            <div id="AdminPanel">
                <div className="StatusBar">
                    <div className="StatusIcon">
                        <h1>Pending</h1>
                    </div>
                    {this.props.AllOrders.pending.map(e => {
                        return <OrderBox OrderStatus={e} key={e.id} />
                    })}
                </div>
                <div className="StatusBar">
                    <div className="StatusIcon">
                        <h1>Preparing</h1>
                    </div>
                    {this.props.AllOrders.preparing.map(e => {
                        return <OrderBox OrderStatus={e} key={e.id} />
                    })}
                </div>
                <div className="StatusBar">
                    <div className="StatusIcon">
                        <h1>Delivered</h1>
                    </div>
                    {this.props.AllOrders.delivered.map(e => {
                        return <OrderBox OrderStatus={e} key={e.id} />
                    })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    AllOrders: state.admin.AllOrders
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    upDateData: () => (dispatch(GetAllOrders()))
})

export default connect(mapStateToProps, mapDispatchToProps)(Admin)