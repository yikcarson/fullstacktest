import React from "react";
import { OrderStatus } from "../../model/AdminOrder";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons'
import { connect } from "react-redux";
import { IRootState, ThunkDispatch } from "../../store";
import { UpdateStatus } from "../../redux/AdminPanel/action";

interface IOrderBoxProps {
    OrderStatus: OrderStatus
    changeStatus: (id: number) => void
}
class OrderBox extends React.Component<IOrderBoxProps> {
    render() {
        return (
            <div className="OrderBox">
                <div className="OrderBoxId"><div>userid:{this.props.OrderStatus.id}</div><span onClick={() => this.props.changeStatus(this.props.OrderStatus.id)}><FontAwesomeIcon icon={faArrowCircleRight} size="lg" /></span></div>
                <div className="OrderBoxHeader"><div>Name</div> <div>Quantity</div></div>
                {
                    this.props.OrderStatus.pizzas.map(e => {
                        return <div className="OrderBoxPizzas"><div>{e.name}</div> <div>{e.quantity}</div></div>
                    })
                }
                <div className="OrderBoxNextButton"></div>
            </div>
        )
    }
}
const mapStateToProps = (state: IRootState) => ({

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    changeStatus: (id: number) => { dispatch(UpdateStatus(id)) }
})
export default connect(mapStateToProps, mapDispatchToProps)(OrderBox)