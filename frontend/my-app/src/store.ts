import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { combineReducers, compose, createStore, applyMiddleware } from 'redux';
import { IPizzasAction } from './redux/Pizzas/actions';
import { pizzasReducer } from './redux/Pizzas/reducer';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { IPizzasState } from './redux/Pizzas/status';
import { IAdminAction } from './redux/AdminPanel/action';
import { IAdminState } from './redux/AdminPanel/state';
import { adminReducer } from './redux/AdminPanel/reducer';

export const history = createBrowserHistory()

type IRootAction = IPizzasAction | IAdminAction

export interface IRootState {
    admin: IAdminState
    pizzas: IPizzasState
    router: RouterState
}

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>
const rootReducer = combineReducers<IRootState>({
    admin: adminReducer,
    pizzas: pizzasReducer,
    router: connectRouter(history)
})
declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    )
)