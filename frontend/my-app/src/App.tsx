import React from 'react';
import './App.css';
import Pizzas from './Components/Pizza/Pizzas';
import { Provider } from 'react-redux';
import store, { history } from './store';
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route } from 'react-router';
import Admin from './Components/AdminPanel/Admin';
import { Navbar } from './Components/Navbar/Navbar';
import Success from './Components/Success/Success';


class App extends React.Component {


  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div className="App">
            <div><Navbar /></div>
            <Switch>
              <div id="Main">
                <Route path='/' exact={true} component={Pizzas} />
                <Route path='/admin' exact={true} component={Admin} />
                <Route path='/order/success' exact={true} component={Success} />
              </div>
            </Switch>

          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
