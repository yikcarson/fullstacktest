import { ClientOrder } from "../../model/pizzaModel";
import { Dispatch } from "redux";

export interface IPizzasSubmit {
    type: "@@IPizzasPostSuccess",
    success: boolean,
    deliveryTime: number,
    redirect: boolean
}


export type IPizzasAction = IPizzasSubmit

export function PostSuccess(success: boolean, deliveryTime: number): IPizzasSubmit {
    return {
        type: "@@IPizzasPostSuccess",
        success,
        deliveryTime,
        redirect: true
    }
}

export function submitOrder(clientorder: ClientOrder[]) {
    return async (dispatch: Dispatch<IPizzasAction>) => {
        const res = await fetch('http://localhost:8080/order/', {
            method: 'Post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(clientorder)
        })
        const result = await res.json()
        if (res.status === 200) {
            dispatch(PostSuccess(result.success, result.deliveryTime))
        }
    }
}