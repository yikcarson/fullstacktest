import { IPizzasAction } from './actions';
import { IPizzasState } from './status';

const initialState: IPizzasState = {
    success: false,
    deliveryTime: 0,
    redirect: false
}

export const pizzasReducer = (state: IPizzasState = initialState, action: IPizzasAction) => {
    switch (action.type) {
        case "@@IPizzasPostSuccess":
            return {
                ...state,
                success: true,
                deliveryTime: action.deliveryTime,
                redirect: action.redirect
            }
    }
    return state
}