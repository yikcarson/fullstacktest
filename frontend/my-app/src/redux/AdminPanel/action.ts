import { AllOrders } from "../../model/AdminOrder";
import { Dispatch } from "redux";

export interface IGetAllOrders {
    type: "@@IGetAllOrders/Success",
    AllOrders: AllOrders
}

export type IAdminAction = IGetAllOrders


export function GetAllOrdersSuccess(AllOrders: AllOrders): IGetAllOrders {
    return {
        type: "@@IGetAllOrders/Success",
        AllOrders
    }
}

export function GetAllOrders() {
    return async (dispatch: Dispatch<IAdminAction>) => {
        const res = await fetch('http://localhost:8080/admin/orders', {
            method: "Get"
        })
        const result = await res.json()
        if (res.status === 200) {
            dispatch(GetAllOrdersSuccess(result))
        }
    }
}
export function UpdateStatus(id: number) {
    return async (dispatch: Dispatch<IAdminAction>) => {
        const res = await fetch(`http://localhost:8080/admin/orders/status/${id}`, {
            method: "PUT"
        })
        const result =await res.json()
        if (res.status === 200) {
            dispatch(GetAllOrdersSuccess(result))
        }
    }
}