import { IAdminState } from "./state";
import { OrderStatus } from "../../model/AdminOrder";
import { IAdminAction } from "./action";

const initialstate: IAdminState = {
    AllOrders: {
        delivered: [] as OrderStatus[],
        pending: [] as OrderStatus[],
        preparing: [] as OrderStatus[],
    }
}


export const adminReducer = (state: IAdminState = initialstate, action: IAdminAction) => {
    switch (action.type) {
        case "@@IGetAllOrders/Success":
            return {
                ...state,
                AllOrders: action.AllOrders
            }
    }
    return state
}