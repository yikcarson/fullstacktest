import { ClientOrder } from "./pizzaModel";

export interface OrderStatus {
    id: number,
    pizzas: ClientOrder[],
    status: "delivered" | "pending" | "preparing"
}




export interface AllOrders {
    delivered: OrderStatus[],
    pending: OrderStatus[],
    preparing: OrderStatus[],
}