export interface pizza {
    name: string,
    price: number,
    ingredients: string[],
    quantity:number
}
export interface ClientOrder {
    name: string,
    quantity: number

}