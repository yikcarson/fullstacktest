import { ClientOrder, AllOrder } from "../model/AllOrders";
import * as fs from 'fs'

export class ClientOrderService {
    add = async (clientOrder: ClientOrder[]) => {
        const AllOrders: AllOrder[] = JSON.parse(await fs.promises.readFile('./orders.json', 'utf-8'))
        const id: number = AllOrders.length
        AllOrders.push({ id: id + 1, pizzas: clientOrder, status: "pending" })
        fs.promises.writeFile('./orders.json', JSON.stringify(AllOrders))
        const resJson = JSON.parse(await fs.promises.readFile('./orders.json', 'utf-8'))
        return resJson
    }
}