import * as fs from 'fs'
import { AllOrder } from '../model/AllOrders';

export class AdminOrderService {
    get = async () => {
        const adminOrders: AllOrder[] = JSON.parse(await fs.promises.readFile('./orders.json', 'utf-8'))
        const classificationOrder = {
            delivered: [] as AllOrder[],
            preparing: [] as AllOrder[],
            pending: [] as AllOrder[]
        }
        adminOrders.forEach(element => {
            if (element.status == "delivered") {
                classificationOrder.delivered.push(element)
            }
            if (element.status == "preparing") {
                classificationOrder.preparing.push(element)
            }
            if (element.status == "pending") {
                classificationOrder.pending.push(element)
            }
        });
        return classificationOrder
    }

    updateStatus = async (id: number) => {
        const adminOrders: AllOrder[] = JSON.parse(await fs.promises.readFile('./orders.json', 'utf-8'))
        const haveOrder = adminOrders.find(e => e.id === id)
        if (haveOrder) {
            if (haveOrder.status == "pending") { haveOrder.status = "preparing" } else
                if (haveOrder.status == "preparing") { haveOrder.status = "delivered" } else
                    if (haveOrder.status == "delivered") { haveOrder.status = "delivered" }
        }
        await fs.promises.writeFile('./orders.json', JSON.stringify(adminOrders))
        const resJson = await this.get()
        return resJson
    }
}