import *as fs from "fs";

export class PizzasService {
    async get() {
        const pizzas = await fs.promises.readFile('./pizzas.json', 'utf-8')
        return JSON.parse(pizzas)
    }

}

