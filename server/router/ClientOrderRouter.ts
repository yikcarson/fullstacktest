import * as express from 'express'
import { ClientOrderService } from '../service/ClientOrderService';

export class ClientOrderRouter {
    constructor(private clientOrderService: ClientOrderService) {

    }
    router = () => {
        const router = express.Router()
        router.post('/', this.orders)
        return router
    }

    orders = async (req: express.Request, res: express.Response) => {
        try {
            const pizzas = await this.clientOrderService.add(req.body)
            return res.status(200).json(pizzas)
        } catch (error) {
            return res.status(401).json(error)
        }

    }
}