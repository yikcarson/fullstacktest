import * as express from 'express'
import { AdminOrderService } from '../service/AdminOrdersService';

export class AdminOrderRouter {
    constructor(private AllOrderService: AdminOrderService) {

    }
    router = () => {
        const router = express.Router()
        router.get('/orders', this.getAllorders)
        router.put('/orders/status/:id', this.updateStatus)
        return router
    }

    getAllorders = async (req: express.Request, res: express.Response) => {
        try {
            const pizzasAllorders = await this.AllOrderService.get()
            return res.status(200).json(pizzasAllorders)
        } catch (error) {
            return res.status(401).json(error)
        }
    }

    updateStatus = async (req: express.Request, res: express.Response) => {
        try {
            const updateOrders = await this.AllOrderService.updateStatus(parseInt(req.params.id))
            return res.status(200).json(updateOrders)
        } catch (error) {
            return res.status(401).json(error)
        }
    }


}