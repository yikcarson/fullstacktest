import * as express from 'express'
import { PizzasService } from '../service/PizzasService';

export class PizzasRouter {
    constructor(private pizzasService: PizzasService) {

    }
    router = () => {
        const router = express.Router()
        router.get('/', this.getPizzas)
        return router
    }

    getPizzas = async (req: express.Request, res: express.Response) => {
        const pizzas = await this.pizzasService.get()
        return res.json(pizzas)
    }


}