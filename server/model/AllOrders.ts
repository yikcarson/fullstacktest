

export interface ClientOrder {
    name: string,
    quantity: number

}
export interface AllOrder {
    id: number,
    pizzas: ClientOrder[],
    status: "delivered" | "preparing" | "pending"
}
