import * as express from 'express'
import { PizzasService } from './service/PizzasService';
import { PizzasRouter } from './router/PizzasRouter';
import { ClientOrderService } from './service/ClientOrderService';
import { ClientOrderRouter } from './router/ClientOrderRouter';
import * as cors from 'cors'
import * as  bodyParser from 'body-parser'
import { AdminOrderService } from './service/AdminOrdersService';
import { AdminOrderRouter } from './router/AdminOrdersRouter';
const app = express()
app.use(cors())
app.use(bodyParser.json())

const pizzaservice = new PizzasService()
const clientorderService = new ClientOrderService()
const adminorderService = new AdminOrderService()

const pizzarouter = new PizzasRouter(pizzaservice)
const clientOrderRouter = new ClientOrderRouter(clientorderService)
const adminorderrouter = new AdminOrderRouter(adminorderService)


app.use('/pizzas', pizzarouter.router())
app.use('/admin', adminorderrouter.router())
app.use('/order', clientOrderRouter.router())

app.listen(8080, () => {
    console.log('server is start ')
})